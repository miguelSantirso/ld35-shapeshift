﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainLogic : MonoBehaviour
{
	[System.Serializable]
	public struct SavedData
	{
		public int bestScore;
	}

	public int score = 0;
	public int level = 0;
	public AudioClip[] shapeSounds;
	public string saveFilename = "save.shape";
	public bool forceMobileMode = false;
	public bool autoPlayer = false;
	[Header("References")]
	public SocialLogic socialLogic;
	public GameObject mainMenuPrefab;
	public GameObject gameOverParticlesPrefab;
	[SerializeField]
	private GameObject[] shapeFeedbackPrefabs;
	public Transform canvasContainer;
	public EnemySpawner spawner;
	public PlayerShape player;
	public BackgroundFlasher bgFlasher;
	public SavedData savedData;

	private bool gameOver = false;

	public bool IsMobileMode {
		get { return forceMobileMode || Application.isMobilePlatform; }
	}
	public int BestScore {
		get { return savedData.bestScore; }
	}
	
	public void OnShapeFailed(EnemyShape shape)
	{
		gameOver = true;
		spawner.enabled = false;
		player.enableInput = false;

		var particles = Instantiate<GameObject>(gameOverParticlesPrefab);
		particles.GetComponent<LineParticles>().SetColor(shape.Color);
		bgFlasher.FlashFromColor(shape.Color);
	}
	public void OnShapeSucceeded(EnemyShape shape)
	{
		score += 1;
		bgFlasher.FlashFromColor(Color.white);
		AudioSource.PlayClipAtPoint(shapeSounds[shape.meshIndex], Vector3.zero);
		
		var feedbackPrefab = shapeFeedbackPrefabs[shape.meshIndex];
		var playerFeedback = Instantiate(feedbackPrefab);
		playerFeedback.GetComponent<MeshRenderer>().material.color = shape.Color;
	}

	void Start ()
	{
		savedData = Pncil.LoadSave<SavedData>.Load(saveFilename);
		StartCoroutine(GameLogic());
	}
	
	IEnumerator GameLogic()
	{
		while (true)
		{
			spawner.enabled = false;
			player.enableInput = true;

			var mainMenu = Instantiate<GameObject>(mainMenuPrefab);
			mainMenu.transform.SetParent(canvasContainer, false);
			yield return new WaitWhile(() => mainMenu != null);

			score = 0;
			level = 0;

			spawner.enabled = true;
			player.enableInput = true;
			gameOver = false;

			spawner.Reset();

			while (!gameOver)
			{
				spawner.SpawnLevel(level);

				while (!gameOver && !spawner.LevelFinished)
				{
					if (autoPlayer)
					{
						var nextShape = NextShapeIndex();
						if (nextShape > 0 && player.Shape != nextShape)
						{
							player.ShapeShift(nextShape);
						}
					}

					yield return null;
				}
				
				if (!gameOver)
				{
					level += 1;
					//yield return new WaitForSeconds(0.35f);
				}
			}

			socialLogic.ReportScore(score);
			savedData.bestScore = System.Math.Max(savedData.bestScore, score);
			Pncil.LoadSave<SavedData>.Save(saveFilename, savedData);

			player.enableInput = false;
			player.ShapeShift(0);

			yield return new WaitForSeconds(1.25f);
		}
	}

	int NextShapeIndex()
	{
		var allEnemies = new List<EnemyShape>(Component.FindObjectsOfType<EnemyShape>());
		allEnemies.Sort((a, b) => a.transform.localScale.x.CompareTo(b.transform.localScale.x));
		return allEnemies.Count > 0 ? allEnemies[0].meshIndex + 1 : -1;
	}


}
