﻿using UnityEngine;
using System.Collections;

public class EnemyShape : MonoBehaviour 
{
	public float startScale = 15f;
	public float pointsPerSecond;
	public int meshIndex;
	public Mesh[] meshes;
	public Material[] materials;
	public MeshFilter meshFilter;

	private MainLogic mainLogic;
	private PlayerShape player;

	public Mesh Mesh { get { return meshes[meshIndex]; } }
	public Color Color { get{ return materials[meshIndex].color;}}

	void Awake()
	{
		mainLogic = Component.FindObjectOfType<MainLogic>();
		player = Component.FindObjectOfType<PlayerShape>();
	}

	void Start()
	{
		GetComponent<MeshRenderer>().material = materials[meshIndex];
		meshFilter.mesh = meshes [meshIndex];
		transform.localScale = new Vector3 (startScale, startScale, startScale);
	}

	void Update ()
	{
		var sc = transform.localScale;
		sc -= new Vector3 (pointsPerSecond, pointsPerSecond, pointsPerSecond) * Time.deltaTime;
		transform.localScale = sc;

		if (sc.x <= 0.85f)
		{
			mainLogic.OnShapeFailed(this);
			Destroy(gameObject);
		}
		else if (sc.x <= 1.15)
		{
			if (player.Shape == meshIndex + 1)
			{
				mainLogic.OnShapeSucceeded(this);
				Destroy(gameObject);
			}
		}
			
		var pos = transform.position;
		pos.z += pointsPerSecond * Time.deltaTime;
		transform.position = pos;
	}
}
