﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EnemySpawner : MonoBehaviour 
{
	public int numShapes = 4;
	public float minSpeed = 2f;
	public float maxSpeed = 10f;
	public float minDelay = 0.15f;
	public float maxDelay = 0.8f;
	public int maxLevel = 15;
	public GameObject shapePrefab;

	public bool LevelFinished { get { return delays.Count == 0; } }

	private List<float> delays = new List<float>();
	private float speed;

	private List<int> availableShapes = new List<int>();
	private List<int> unusedShapes = new List<int>();

	public void Reset()
	{
		availableShapes.Clear();
		unusedShapes = new List<int>{0,1,2,3};
		AddAvailableShape();
		AddAvailableShape();
	}

	public void SpawnLevel(int level)
	{
		if (level == 2)
			AddAvailableShape();
		else if (level == 4)
			AddAvailableShape();
		
		delays.Clear();

		var numGroups = 1;//0 ? 1 : Random.Range(1, 3);
		var t = level/(float)maxLevel;
		var groupItemDelay = Mathf.LerpUnclamped(maxDelay, minDelay, t);
		speed = level == 0 ? 2f : Mathf.LerpUnclamped(minSpeed, maxSpeed, t);

		for (var i = 0; i < numGroups; ++i)
		{
			var shapesInGroup = 1;
			if (level == 0)
				shapesInGroup = 1;
			else if (level < 4)
				shapesInGroup = Random.Range(5, 10);
			else if (level < 6)
				shapesInGroup = Random.Range(9, 12);
			else 
				shapesInGroup = Random.Range(12, 15);
			
			for (var j = 0; j < shapesInGroup; ++j)
			{
				delays.Add(Random.Range(0, 2) == 0 ? groupItemDelay : 1.5f * groupItemDelay);
			}
			if (level < 4)
				delays.Add(10f / speed);
		}
	}

	void OnDisable()
	{
		var allEnemies = Component.FindObjectsOfType<EnemyShape>();
		foreach (var enemy in allEnemies)
			Destroy(enemy.gameObject);
	}

	void Update()
	{
		if (delays.Count > 0)
		{
			delays[0] -= Time.deltaTime;
			if (delays[0] <= 0)
			{
				delays.RemoveAt(0);

				if (delays.Count > 0)
				{
					var shapeGameObj = Instantiate<GameObject>(shapePrefab);
					var controller = shapeGameObj.GetComponent<EnemyShape>();
					controller.meshIndex = availableShapes[Random.Range(0, availableShapes.Count)];
					controller.pointsPerSecond = speed;
				}
			}
		}
	}

	void AddAvailableShape()
	{
		if (unusedShapes.Count > 0)
		{
			var randIndex = Random.Range(0, unusedShapes.Count);
			availableShapes.Add(unusedShapes[randIndex]);
			unusedShapes.RemoveAt(randIndex);	
		}
	}
	
}
