﻿using UnityEngine;
using System.Collections;

public class MoreGamesLink : MonoBehaviour {

	public void OpenMoreGames()
	{
		if (Application.platform == RuntimePlatform.Android)
			Application.OpenURL("market://search?q=pub:pncil.com+›");
		else if (Application.platform == RuntimePlatform.IPhonePlayer)
			Application.OpenURL("http://appstore.com/miguelsantirsogonzalez");
	}
}
