﻿using UnityEngine;
using UnityEngine.SocialPlatforms;

public class SocialLogic : MonoBehaviour
{
	#region inspector properties
	[SerializeField]
	private Transform leaderboardButton;
	#endregion

	public void ReportScore(int score)
	{
		if (Social.localUser.authenticated)
		{
			Social.ReportScore(score, "highscore", null);
		}
	}

	public void OpenLeaderboard()
	{
		if (Social.localUser.authenticated)
		{
			Social.ShowLeaderboardUI();
		}
	}

	void Start()
	{
		if (leaderboardButton)
			leaderboardButton.gameObject.SetActive(false);
		Social.localUser.Authenticate(ProcessAuthentication);
	}
	
	void ProcessAuthentication(bool success)
	{
		if (leaderboardButton)
			leaderboardButton.gameObject.SetActive(success);
	}
}
