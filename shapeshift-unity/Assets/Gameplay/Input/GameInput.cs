﻿using UnityEngine;
using System.Collections.Generic;

public class GameInput : MonoBehaviour 
{
	public List<string> directionButtons = new List<string> { "Up", "Right", "Down", "Left" };
	public GameObject mobileTouchDetector;
	public MainLogic mainLogic;

	private delegate int ShapeSelector();

	private ShapeSelector selectShape;
	private int selectedShape = 0;

	public int SelectedShape { get { return selectedShape; } }
	
	public void OnShapeAreaTouched(int shape)
	{
		selectedShape = shape;
	}
	public void OnShapeAreaReleased(int shape)
	{
		if (shape == selectedShape)
			selectedShape = 0;
	}
	
	void Start()
	{
		selectShape = SelectShapeOnDesktop;
		if (mainLogic.IsMobileMode)
		{
			selectShape = SelectShapeOnMobile;
		}
		else 
		{
			Destroy(mobileTouchDetector);
		}
	}

	void Update()
	{
		selectedShape = selectShape();
	}

	private int SelectShapeOnDesktop()
	{
		var justPressedButton = directionButtons.FindIndex(d => Input.GetButtonDown(d));
		return justPressedButton >= 0 ? justPressedButton + 1 : 0;
	}

	private int SelectShapeOnMobile()
	{
		return selectedShape;
	}
}
