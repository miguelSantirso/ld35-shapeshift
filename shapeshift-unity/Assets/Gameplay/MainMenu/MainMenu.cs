﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour 
{
	public GameObject desktopTutorialPrefab;
	public GameObject mobileTutorialPrefab;
	public Text lastScore;
	public Text bestScore;
	public Animator[] scoreLabelsAnimators;

	private Animator[] inputHints;

	private MainLogic mainLogic;
	private GameInput gameInput;
	private SocialLogic socialLogic;

	public void OnHighscoreClicked()
	{
		socialLogic.OpenLeaderboard();
	}

	void Awake()
	{
		mainLogic = FindObjectOfType<MainLogic>();
		gameInput = FindObjectOfType<GameInput>();
		socialLogic = FindObjectOfType<SocialLogic>();
	}

	void Start()
	{
		var tutorialPrefab = mainLogic.IsMobileMode ? mobileTutorialPrefab : desktopTutorialPrefab;
		var tutorialGameObj = Instantiate(tutorialPrefab);
		tutorialGameObj.transform.SetParent(transform, false);

		inputHints = tutorialGameObj.GetComponentsInChildren<Animator>();
		
		lastScore.text = mainLogic.score.ToString();
		bestScore.text = mainLogic.BestScore.ToString();

		foreach (var scoreLabelAnimator in scoreLabelsAnimators)
		{
			scoreLabelAnimator.SetBool("Visible", mainLogic.BestScore > 0);
		}

		StartCoroutine(HintBlinks());
	}

	void Update()
	{
		var shapeAnimatorIndex = gameInput.SelectedShape - 1;
		if (shapeAnimatorIndex >= 0 && inputHints[shapeAnimatorIndex])
		{
			inputHints[shapeAnimatorIndex].SetBool("Visible", false);
			inputHints[shapeAnimatorIndex] = null;
		}

		if (System.Array.TrueForAll(inputHints, h => h == null))
		{
			enabled = false;
			StopAllCoroutines();
			StartCoroutine(DelayedDestroy());
		}
	}

	IEnumerator HintBlinks()
	{
		while (true)
		{
			yield return new WaitForSeconds(2.5f);

			var remainingHints = System.Array.FindAll(inputHints, h => h != null);
			foreach (var hint in remainingHints)
			{
				hint.SetTrigger("Blink");
				yield return new WaitForSeconds(0.25f);
			}
		}
	}

	IEnumerator DelayedDestroy()
	{
		yield return new WaitForSeconds(0.3f);
		Destroy(gameObject);
	}
}
