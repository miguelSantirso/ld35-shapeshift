﻿using UnityEngine;
using System.Collections;

public class MeshMorpher : MonoBehaviour
{
	public Mesh[] meshes;
	public float transitionSeconds;
	public MeshFilter targetMeshFilter;

	private int meshAnimationTarget = -1;
	private float animationSecondsLeft = 0;

	private Mesh objectMesh;
	private Vector3[] animationStartVertices;

	public void MorphToMesh(int meshIndex)
	{
		if (meshAnimationTarget != meshIndex)
		{
			animationSecondsLeft = transitionSeconds;
			meshAnimationTarget = meshIndex;
			animationStartVertices = objectMesh.vertices;
		}
	}

	void Awake()
	{
		objectMesh = targetMeshFilter.mesh;
	}

	void Update()
	{
		if (animationSecondsLeft > 0) 
		{
			animationSecondsLeft -= Time.deltaTime;
			var t = Mathf.Clamp01(1f - animationSecondsLeft / transitionSeconds);
			LerpMesh (animationStartVertices, meshes [meshAnimationTarget].vertices, t);
		}
	}

	void LerpMesh(Vector3[] fromVertices, Vector3[] toVertices, float t)
	{
		var numVertices = objectMesh.vertexCount;
		var result = new Vector3[numVertices];
		for (var i = 0; i < numVertices; i++)
			result[i] = Vector3.Lerp(fromVertices[i], toVertices[i], t);

		objectMesh.vertices = result;
		objectMesh.RecalculateBounds ();
	}
}
