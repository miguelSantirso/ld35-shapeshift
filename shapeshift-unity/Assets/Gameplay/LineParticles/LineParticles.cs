﻿using UnityEngine;
using System.Collections;

public class LineParticles : MonoBehaviour 
{
	public void SetColor(Color color)
	{
		var trails = GetComponentsInChildren<TrailRenderer>();
		foreach (var t in trails)
		{
			var material = t.material;
			material.color = color;
			material.SetColor("_TintColor", color);
			t.material = material;
		}
	}

	public void OnAnimationFinished()
	{
		Destroy(gameObject);
	}

}
