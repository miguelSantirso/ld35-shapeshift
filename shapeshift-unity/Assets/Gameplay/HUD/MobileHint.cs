﻿using UnityEngine;
using System.Collections;

public class MobileHint : MonoBehaviour 
{
	#region inspector properties
	[SerializeField]
	private MainLogic mainLogic;
	[SerializeField]
	private Animator animator;
	#endregion

	void Awake () 
	{
		if (!mainLogic.IsMobileMode)
			Destroy(gameObject);
	}
	
	void Update()
	{
		animator.SetInteger("Score", mainLogic.score);
	}

}
