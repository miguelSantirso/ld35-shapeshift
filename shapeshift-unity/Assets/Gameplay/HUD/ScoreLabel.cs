﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreLabel : MonoBehaviour
{
	public Text label;
	public MainLogic mainLogic;

	void Update () 
	{
		label.text = mainLogic.score.ToString();
	}
}
