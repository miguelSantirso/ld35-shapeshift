﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelLabel : MonoBehaviour
{
	public Text label;
	public MainLogic mainLogic;

	void Update()
	{
		label.text = string.Format("level {0}", mainLogic.level+1);
	}
}
