﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerShape : MonoBehaviour 
{
	public Color[] colors;
	public float colorAnimationSeconds = 0.075f;
	public Material material;
	public MeshMorpher meshMorpher;
	public bool enableInput = false;

	private int currentShape = 0;
	private Color colorAnimationStart;
	private Color colorAnimationEnd;
	private float colorAnimationTimeLeft;
	private GameInput gameInput;

	public int Shape
	{
		get { return currentShape; }
	}

	public void ShapeShift(int shape)
	{
		meshMorpher.MorphToMesh(shape);
		colorAnimationStart = material.color;
		colorAnimationEnd = colors[shape];
		colorAnimationTimeLeft = colorAnimationSeconds;

		currentShape = shape;
	}

	void Awake()
	{
		gameInput = Component.FindObjectOfType<GameInput>();
	}

	void Start()
	{
		meshMorpher.MorphToMesh(0);
		material.color = colors[0];
	}

	void Update()
	{
		var selectedShape = gameInput.SelectedShape;
		if (enableInput && selectedShape > 0)
		{
			ShapeShift(selectedShape);
		}

		if (colorAnimationTimeLeft > 0)
		{
			colorAnimationTimeLeft -= Time.deltaTime;
			var t = Mathf.Clamp01(1f - colorAnimationTimeLeft / colorAnimationSeconds);
			material.color = Color.Lerp(colorAnimationStart, colorAnimationEnd, t);
		}

	}
	
}
