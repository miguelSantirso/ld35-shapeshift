﻿using UnityEngine;
using System.Collections;

public class CameraResponsiveSize : MonoBehaviour 
{
	#region inspector properties
	[SerializeField]
	private Vector2 referenceResolution;
	[SerializeField]
	private Camera theCamera;
	#endregion

	private float originalOrthoSize;

	private Vector2 currentScreenSize;

	void Start()
	{
		originalOrthoSize = theCamera.orthographicSize;

		Update();
	}
	
	void Update()
	{
		if (currentScreenSize.x != Screen.width || currentScreenSize.y != Screen.height)
		{
			currentScreenSize.x = Screen.width;
			currentScreenSize.y = Screen.height;

			float scaleX = currentScreenSize.x / referenceResolution.x;
			float scaleY = currentScreenSize.y / referenceResolution.y;
			float scaleFactor = Mathf.Max(1f, scaleY / scaleX);
			theCamera.orthographicSize = originalOrthoSize * scaleFactor;
		}
	}
	
}
