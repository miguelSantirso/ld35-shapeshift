﻿using UnityEngine;
using System.Collections;

public class IntroShape : MonoBehaviour 
{
	public Color[] colors;
	public float colorAnimationSeconds = 0.075f;
	public Material material;
	public MeshMorpher meshMorpher;

	private int currentShape = 0;
	private Color colorAnimationStart;
	private Color colorAnimationEnd;
	private float colorAnimationTimeLeft;

	public int Shape
	{
		get { return currentShape; }
	}

	public void ShapeShift(int shape)
	{
		meshMorpher.MorphToMesh(shape);
		colorAnimationStart = material.color;
		colorAnimationEnd = colors[shape];
		colorAnimationTimeLeft = colorAnimationSeconds;

		currentShape = shape;
	}

	void Start()
	{
		meshMorpher.MorphToMesh(0);
		material.color = colors[0];
	}

	void Update()
	{
		if (colorAnimationTimeLeft > 0)
		{
			colorAnimationTimeLeft -= Time.deltaTime;
			var t = Mathf.Clamp01(1f - colorAnimationTimeLeft / colorAnimationSeconds);
			material.color = Color.Lerp(colorAnimationStart, colorAnimationEnd, t);
		}
	}
}
