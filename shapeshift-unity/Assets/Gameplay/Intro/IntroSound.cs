﻿using UnityEngine;
using System.Collections;

public class IntroSound : MonoBehaviour 
{
	#region inspector properties
	[SerializeField]
	private AudioSource audioSource;
	#endregion

	public void StartPlaying()
	{
		StartCoroutine (PlaySoundCoroutine ());
	}

	void Start () 
	{
		DontDestroyOnLoad (gameObject);
	}

	IEnumerator PlaySoundCoroutine()
	{
		audioSource.Play ();

		while (audioSource.isPlaying)
			yield return null;

		Destroy (gameObject);
	}

}
