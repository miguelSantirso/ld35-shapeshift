﻿using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;
using System.Collections.Generic;

public class IntroSequence : MonoBehaviour 
{
	#region inspector properties
	[SerializeField]
	private IntroShape shape;
	[SerializeField]
	private int numShapes = 50;
	[SerializeField]
	private float skipDelaySeconds = 4f;
	[SerializeField]
	private Animator introLetters;
	#endregion

	private bool clickReceived = false;

	public void OnClicked()
	{
		clickReceived = true;
	}

	void Start()
	{
		StartCoroutine(IntroSequenceCoroutine());
	}

	IEnumerator IntroSequenceCoroutine()
	{
        while (!SplashScreen.isFinished)
			yield return null;

		introLetters.SetTrigger ("Start");

		var meshMorpher = shape.GetComponent<MeshMorpher>();

		var shapes = new List<int> { 1, 2, 3, 4 };
		for (int i = 0; i < numShapes; ++i)
		{

			var t = i / (float)numShapes;
			var easedT = Pncil.Easing.EaseIn(t, Pncil.EasingType.Quartic);
			var delay = Mathf.Lerp(0.1f, 0.5f, easedT);

			meshMorpher.transitionSeconds = delay;
			shape.ShapeShift(shapes[i % shapes.Count]);

			yield return new WaitForSeconds(delay);
		}

		shape.ShapeShift(3);

		var endTime = Time.time + skipDelaySeconds;
		while (Time.time < endTime && !clickReceived) yield return null;

		UnityEngine.SceneManagement.SceneManager.LoadScene("GameplayScene");
	}
}
