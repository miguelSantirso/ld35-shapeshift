﻿using UnityEngine;
using System.Collections;

public class BackgroundFlasher : MonoBehaviour 
{
	public float animationSeconds;
	public Camera backgroundCamera;

	private Color originalColor;

	private float animationTimeLeft;
	private Color animationStartColor;

	public void FlashFromColor(Color color)
	{
		animationStartColor = color;
		animationTimeLeft = animationSeconds;
	}

	void Start () 
	{
		originalColor = backgroundCamera.backgroundColor;
	}

	void Update () 
	{
		if (animationTimeLeft > 0)
		{
			animationTimeLeft -= Time.deltaTime;
			var t = 1f - Mathf.Clamp01(animationTimeLeft / animationSeconds);
			backgroundCamera.backgroundColor = Color.Lerp(animationStartColor, originalColor, t);
		}
	}
}
